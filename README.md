# Тестовый проект grape-framework

Данный проект используется для разработки ранней версии Grape Framework. Чтобы запустить проект на компьютере разработчика, используйте следующую инструкцию.

## Настройка рабочего окружения

Для того чтобы подключиться к работе над проектом Grape Framework, необходимо сначала скачать проект себе на компьютер и настроить рабочее окружение, установив все необходимые инструменты разработчика. Исходим из того, что на компьютере разработчика установлена Ubuntu 14.04 LTS 64-bit.

Сначала устанавливаем [Docker](http://www.docker.com/):

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sudo sh -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
sudo apt-get update
sudo apt-get install lxc-docker cgroup-lite
```

Чтобы разрешить запуск Docker от имени обычного пользователя, используется следующая команда:

```
sudo usermod -a -G docker $USER
``` 

Чтобы установка прав сработала, необходимо выйти из системы и зайти вновь.

Далее установим [Fig](http://orchardup.github.io/fig/index.html):

```
sudo wget https://github.com/orchardup/fig/releases/download/0.4.2/linux -O /usr/local/bin/fig
sudo chmod +x /usr/local/bin/fig
```

## Устанавливаем и запускаем тестовый проект

Скачиваем последнюю версию исходников и переходим в папку:

```
cd Projects
git clone https://korkholeh@bitbucket.org/korkholeh/grape_frwk_project.git grape_framework
cd grape_framework
```

Для запуска проекта достаточно исполнить команду:

```
fig up
```

При первом запуске вы получите сообщение о невозможности подключиться к БД `sitedb`. Это связано с тем, что такая БД еще не создана. Не прерывая работу предыдущей команды, откройте еще один терминал и перейдите в консоль контейнера:

```
fig run web bash
```

Далее, запустите консоль PostgreSQL:

```
psql -h $DB_1_PORT_5432_TCP_ADDR postgres postgres
```

Теперь создайте новую БД:

```
postgres=# create database sitedb;
```

Используйте `\q` для выхода из консоли PostgreSQL и `Ctrl+D` для выхода из терминала контейнера.

Теперь можно запускать создание необходимых таблиц:

```
fig run web python3 website/manage.py migrate
```

После того как все необходимые таблицы создадутся, вернитесь назад к первому терминалу и перезапустите `fig up`. Теперь проект должен быть успешно доступен в вашей системе по адресу [http://localhost:8090](http://localhost:8090).

Создадим также учетную запись суперпользователя:

```
fig run web python3 website/manage.py createsuperuser
```
