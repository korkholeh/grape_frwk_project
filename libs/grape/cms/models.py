from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey


class Page(MPTTModel):
    active = models.BooleanField(_('Active'), default=False)
    title = models.CharField(_('Title'), max_length=300)
    slug = models.SlugField(_('Slug'), unique=True, max_length=300)
    parent = TreeForeignKey(
        'self', null=True, blank=True, related_name='children')
    position = models.IntegerField(_('Position'), default=999)

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')

    def __unicode__(self):
        return self.title
