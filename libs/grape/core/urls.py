from django.conf.urls import patterns, url
from django.conf import settings

from grape.core.views import page


if settings.APPEND_SLASH:
    regex = r'^(?P<slug>[0-9A-Za-z-_.//]+)/$'
else:
    regex = r'^(?P<slug>[0-9A-Za-z-_.//]+)$'

urlpatterns = patterns(
    '',
    url(regex, page, name='page-by-slug'),
    url(r'^$', page, {'slug': ''}, name='page-root'),
)
