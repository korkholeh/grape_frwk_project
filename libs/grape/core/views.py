from django.http import HttpResponse
from django.conf import settings


def page(request, slug):
    """The main view of the Grape Frameworks. Takes a request and a
    slug, renders the page.
    """
    if slug == '':
        return HttpResponse('<html><head></head><body>'+str(settings.SITE_ID)+'::ROOT</body></html>')
    else:
        return HttpResponse('<html><head></head><body>'+str(settings.SITE_ID)+'::'+slug+'</body></html>')
