# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import grape.multisite.models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alias',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('domain', models.CharField(max_length=100, unique=True, help_text='Either "domain" or "domain:port"', verbose_name='domain name')),
                ('is_canonical', models.NullBooleanField(editable=False, default=None, help_text='Does this domain name match the one in site?', verbose_name='is canonical?', validators=[grape.multisite.models.validate_true_or_none])),
                ('redirect_to_canonical', models.BooleanField(verbose_name='redirect to canonical?', help_text='Should this domain name redirect to the one in site?', default=True)),
                ('site', models.ForeignKey(to='sites.Site')),
            ],
            options={
                'verbose_name_plural': 'aliases',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='alias',
            unique_together=set([('is_canonical', 'site')]),
        ),
    ]
