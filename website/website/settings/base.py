from django.utils.translation import ugettext_lazy as _
from grape.multisite import SiteID
import os

BASE_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), '..')

SECRET_KEY = 'wbiqwd2ha$d)n+5(9ob5jwbl)4f+b&2$p3p(tpw1q+$s8itpp!'

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

SITE_ID = SiteID(default=1)
# SITE_ID = 1

# Application definition

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django_extensions',
    'mptt',
    'grape.core',
    'grape.multisite',
)

MIDDLEWARE_CLASSES = (
    'grape.multisite.middleware.CookieDomainMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'grape.multisite.middleware.DynamicSiteMiddleware',
)

ROOT_URLCONF = 'website.urls'

WSGI_APPLICATION = 'website.wsgi.application'

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _(u'English')),
    ('ru', _(u'Russian')),
    ('uk', _(u'Ukrainian')),
)

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# TEMPLATE_DIRS = (
#     os.path.join(BASE_DIR, 'templates'),
# )

# LOCALE_PATHS = (
#     os.path.join(BASE_DIR, 'website'),
# )

PUBLIC_DIR = os.path.join(BASE_DIR, '..', 'public')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')
STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')

LOGIN_REDIRECT_URL = '/'

TEMPLATE_LOADERS = (
    'grape.multisite.template_loader.Loader',
    'django.template.loaders.app_directories.Loader',
)

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'

# CACHE_MULTISITE_ALIAS = 'multisite'

APPEND_SLASH = True
