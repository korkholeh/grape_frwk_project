from .base import *
import os

DEBUG = True
TEMPLATE_DEBUG = DEBUG

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sitedb',
        'USER': 'postgres',
        'PASSWORD': 'qazwsx',
        'HOST': os.environ.get('DB_1_PORT_5432_TCP_ADDR'),
        'PORT': os.environ.get('DB_1_PORT_5432_TCP_PORT'),
    },
}

INSTALLED_APPS += (
    # 'debug_toolbar',
    'debug_toolbar.apps.DebugToolbarConfig',
)
# INTERNAL_IPS = (
#     '127.0.0.1', '0.0.0.0')
MIDDLEWARE_CLASSES = (
    'grape.core.middleware.ProfileMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
) + MIDDLEWARE_CLASSES
# DEBUG_TOOLBAR_PATCH_SETTINGS = False


DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_COLLAPSED': True,
    'SQL_WARNING_THRESHOLD': 100,  # milliseconds
    'SHOW_TOOLBAR_CALLBACK': 'grape.core.utils.get_debug_toolbar_visibility',
}
